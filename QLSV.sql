CREATE TABLE DMKHOA (
    MaKH varchar(6) NOT NULL,
    TenKhoa varchar(30) NOT NULL
);
CREATE TABLE SINHVIEN(
    MaSV varchar(6) NOT NULL,
    HoSV varchar(30) NOT NULL,
    TenSV varchar(15) NOT NULL,
    GioiTinh char(1) NOT NULL,
    NgaySinh DateTime,
    NoiSinh varchar(50) NOT NULL,
    DiaChi varchar(50) NOT NULL,
    MaKH varchar(6) NOT NULL,
    HocBong int NOT NULL
);